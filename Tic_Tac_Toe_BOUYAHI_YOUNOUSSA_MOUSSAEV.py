import random as rd
 
"""
Fonction qui permet d'initialiser une partie en créant le grille de jeu
et en assignant à chaque joueur un motif pour jouer
"""

def initialiser_partie(joueur1):
    grille = [-1,-1,-1,-1,-1,-1,-1,-1,-1] # -1 signifie que la case est vide
    return lancer_partie(grille, joueur1) # On lance la partie


def lancer_partie(grille,joueur1):
 
    joueur2 = "bot" # Deuxième joueur : bot
    gameStatus = 0
    """ 
    # Si game status = 0, la partie est toujours en cours, si game status = 1,
    un joueur a gagné, Si game status = 2, toutes les cases ont été remplis : cas d'égalité
    """
    compteurCasesRemplis = 0
    dernierJoueur = joueur1
    while gameStatus == 0:
        if compteurCasesRemplis%2 == 0:
            #C'est au joueur 1 de jouer
            dernierJoueur = joueur1
        else:
            #C'est au joueur 2 de jouer
            dernierJoueur = joueur2
 
        remplir_case(grille,dernierJoueur,joueur1, joueur2)
 
        compteurCasesRemplis += 1
 
        # On vérifie içi si la partie est terminé (soit cas d'égalité soit un des deux joueurs a gagné)
 
        if verif_partie(grille):
             gameStatus = 1
 
        elif compteurCasesRemplis == 8:
            gameStatus = 2
 
    #La partie est terminée
 

    if gameStatus == 1: #Cas d'une victoire d'un des deux joueurs
        if dernierJoueur == joueur1:
            return 5 #Le joueur 1 remporte la partie
        else:
            return 0 #Le bot remporte la partie
 
    elif gameStatus == 2: #Cas d'égalité
        return 2
 
 
def remplir_case(grille, joueur, joueur1, joueur2) :
    choixCase = rd.randrange(0,8)
    while grille[choixCase] != -1:
        choixCase = rd.randrange(0,8)
 
    #La case est maintenant disponible
    if(joueur == joueur1):
        grille[choixCase] = 0 #On remplit avec des cercles
    elif(joueur == joueur2):
        grille[choixCase] = 1 # On remplit avec des croix


def verif_partie(grille): # On vérifie toutes les conditions de victoire pour chaque lignes/colonnes/diagonales
 
    if grille[0] >= 0 and grille[1] >= 0 and grille[2] >= 0 and grille[0] == grille[1] == grille[2]:
        return True
    elif grille[3] >= 0 and grille[4] >= 0 and grille[5] >= 0 and grille[3] == grille[4] == grille[5]:
        return True
    elif grille[6] >= 0 and grille[7] >= 0 and grille[8] >= 0 and grille[6] == grille[7] == grille[8]:
        return True
 
    elif grille[0] >= 0 and grille[3] >= 0 and grille[6] >= 0 and grille[0] == grille[3] == grille[6]:
        return True
    elif grille[1] >= 0 and grille[4] >= 0 and grille[7] >= 0 and grille[1] == grille[4] == grille[7]:
        return True
    elif grille[2] >= 0 and grille[5] >= 0 and grille[8] >= 0 and grille[2] == grille[5] == grille[8]:
        return True
 
    elif grille[0] >= 0 and grille[4] >= 0 and grille[8] >= 0 and grille[0] == grille[4] == grille[8]:
        return True
    elif grille[2] >= 0 and grille[4] >= 0 and grille[6] >= 0 and grille[2] == grille[4] == grille[6]:
        return True
 
    return False # Si aucune condition de victoire n'a été vérifiée, alors la partie continue
 

noms_personnages = []
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    lignes = f.readlines()
    for ligne in lignes[1:]:
        caracteristiquePersonnage = ligne.split(";")
 
        nom = caracteristiquePersonnage[1]
        noms_personnages.append(nom)
 
 
resultats = {} #Dictionnaire qui va contenir les résultast
 
somme = 0
 
for nom in noms_personnages:
    i = 0
    scoreJoueur = 0
    while i <= 9:
        score = initialiser_partie(nom)
        i = i + 1
        scoreJoueur += score
    resultats[nom] = scoreJoueur
    somme += scoreJoueur
 
moyenne = somme/len(resultats)
classementSelonpoints = (list(resultats.items()))

classementTrie = sorted(classementSelonpoints, key=lambda tup: (tup[1]))
for resultat in classementTrie:
    print("Voici le résultat du joueur après 10 partie de Tic-Tac-Toc:", resultat)   
print("La moyenne des scores est de", moyenne)
